# disturbskyvis

Based on the code of Cristina-Maria Cordun.

Calculate visibilities using 
- simulated antenna radiation pattern (from Michel Arts in .mat format)
- baselines between antennes
- sky map using [PyGSM](https://github.com/telegraphic/PyGSM)

